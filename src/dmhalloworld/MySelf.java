package dmhalloworld;

public class MySelf {
	
	private int age;
	private String firstname;
	private String surname;
	private String goal;
        private String profile;
        private String hallomsg;
        private String skills;
        private String secondname;
	
	public MySelf() {
		age = 22;
		firstname = "Elizabeth";
                secondname = "Lizzy";
		goal = "Make Zimbabwe a better nation through Artificial Intelligence and \n" + "put a smile on every face.";
		surname = "Matsauke.";	
                skills = "I have attained the following soft skills: \n"+"Working efficiently  as an individual and as part of a team\n" +
                        "Excellent written and verbal communication skills\n" +
                        "Working under pressure\n" +"Keen to learn\n" +"Good thinking skills for problem solving and decision making\n" +
                        "Resilience \n" +"Good self management";
		profile = "I am currently a second year Information Technology Student at Chinhoyi University.\n";
                hallomsg ="Hallo World, I am going to tell you more about myself.";
                
                
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		
		return age;

	}
	        
        public String getHallomsg() {
		
		return hallomsg;
	}
	public String getFirstname() {
		
		return firstname;// returns the first name
	}
        public String getSecondname() {
		
		return secondname;// returns the second name
	}
        public String getSkills() {
		
		return skills;
	}
        /**
	 * @return the profile
	 */
        public String getProfile() {
		
		return profile;
	}
	/**
	 * @return the goal
	 */
	public String getGoal() {
		
		return goal;
	}
	/**
	 * @param surname the surname to set
	 */
	public String getSurname() {
		
		return surname;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MySelf [age=" + age + ", firstname=" + firstname + ", surname=" + surname + ", goal=" + goal + "]";
	}
	

}
